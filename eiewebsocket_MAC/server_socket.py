import socket
import threading

HEADER = 64
PORT = 3074
SERVER = '158.251.91.68'
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = 'DISCONNECT!'
mac_accept='00:1e:c2:9e:28:6b'
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server.bind(ADDR)

def handle_client(conn, addr):
    print(f"[NEW CONNECTION] {addr} connected.")

    connected = True
    while connected:
        msg_length = conn.recv(HEADER).decode(FORMAT)
        if msg_length:
            msg_length = int(msg_length)
            msg = conn.recv(msg_length).decode(FORMAT)
            mac = msg[0:17]
            if mac != mac_accept:
                print('SE HA DETECTADO UNA AMENAZA')
                start()
            if msg[17:] == DISCONNECT_MESSAGE:
                connected = False

            print(f"[{addr}][{mac}] {msg[17:]}")
            conn.send("Msg received".encode(FORMAT))

    conn.close()


def start():
    server.listen()
    print(f"[LISTEN] Server is listening on address {ADDR}")
    while True:
        conn, addr = server.accept()
        thread = threading.Thread(target=handle_client, args=(conn, addr))
        thread.start()
        counter=threading.activeCount()
        print(f"[ACTIVE CONNECTIONS] {counter - 1}")

print("[STARTING] server is running.....")
start()
